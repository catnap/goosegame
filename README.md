# goose-game

**Dependencies**

To run the Goose Game, you will need a java JRE installed on your local machine.





**Mac/Linux**

To start the game, run the following in your terminal 

1. Open the terminal 
2. Set this project as the local directory
3. Run ./startGame.sh





**Windows**
1. Open command line 
2. Set this project as the local directory
3. Run .\startGame.bat




You can get started with the Goose Game by typing in 'help'





**Setting up new Jar**

Note: This demo comes prepackaged with a working jar. No compilation is required


To build a new Jar, you'll need a running SBT environment


1. Open the terminal/command line
2. Set this project as the local directory
3. Run 'sbt assembly'

Your jar will be setup within the target/scala-2.12 directory
Execute with 'java -jar myJarFile.jar'