import scala.collection.mutable

/*
 * Copyright: blackbox-test -- created 24.04.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object PlayerManager {


  private val players = new mutable.HashMap[String, Int]()


  def add( player: String ): Unit = { players += ( player ->  0 ) }
  def move( player: String, position: Int  ): Unit = { players( player ) = position }
  def exists( player: String ): Boolean = {  players.contains( player ) }
  def getPlayers: Iterable[String] = { players.keys }
  def getPlayerPosition( player: String ): Int = { players( player ) }


}
