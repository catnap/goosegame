
/*
 * Copyright: blackbox-test -- created 24.04.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */

import Commands._


object Game {

  // ---------------------  Constructors

  def apply(): Game = {
    new Game()
  }

}

class Game{

  private var FLAG_CONTINUE_GAME: Boolean = true


  def startGame: Unit = {

    TerminalDisplay.info( s"\nWelcome To The Goose Game!\n" )
    TerminalDisplay.info( s"\nType Help to get started\n" )

    while( FLAG_CONTINUE_GAME ){
      continueGame
    }

  }


  def stopGame: Unit = { FLAG_CONTINUE_GAME = false }


  private def continueGame: Unit = {
    try {
      val nextLine = TerminalDisplay.getUserInput
      readString(nextLine)
    } catch {
      case e: Exception => TerminalDisplay.error( "Something went wrong. Try again")
        continueGame
    }
  }




  def movePlayer( playerName: String, die1: Int, die2: Int ): Unit = {

    if(  isValidDie( die1 ) && isValidDie( die2 ) ){

      MoveManager.movePlayer( playerName, die1, die2 )

    } else {
      TerminalDisplay.error( "Dice must be values between 1 and 6")
    }
  }




  def addPlayer( playerName: String ): Unit = {
    if( ! PlayerManager.exists( playerName) ) {
      PlayerManager.add(playerName)
      TerminalDisplay.info(s"players: " + PlayerManager.getPlayers.mkString(", "))
    }
      else {
      TerminalDisplay.info( s"$playerName: already existing player")
    }
  }






  def readString(userInput: String ): Unit = {

    // split on whitespace
    val listWords = userInput.split( "\\s+")
    val listWordsNoCase = listWords.map( _.toLowerCase() )

    listWordsNoCase(0) match {

      case ADD =>
          if( isValidAddUserCommand( listWords ) ) {
            val playerName = listWords(2)
            addPlayer( playerName  )
          }
            else invalid( userInput )

      case MOVE =>

        if( isValidMoveUserCommand( listWords )) {

          val playerName = listWords(1)
          var die1: Int = 0
          var die2: Int = 0

          if (!PlayerManager.exists(playerName)){
            TerminalDisplay.error(s"Player $playerName needs to be added first. Type 'help' for details")
          }
          else {

            if (listWords.length == 2) {
              val random = scala.util.Random
              die1 = random.nextInt(6) + 1
              die2 = random.nextInt(6) + 1
            } else {
              die1 = extractDieFromString( listWords(2) )
              die2 = extractDieFromString( listWords(3) )
            }

            movePlayer(playerName, die1, die2)
          }
        } else invalid( userInput )
      case HELP | QUESTION_MARK => TerminalDisplay.help
      case EXIT => {
        TerminalDisplay.info( "Goodbye!")
        stopGame
      }
      case _ => invalid( userInput )
    }
  }


  private def extractDieFromString( string: String ): Int = { string.replace( "," ,"" ).toInt }
  private def isValidDie( die: Int ): Boolean = { die > 0 && die < 7   }




  private def isValidAddUserCommand( userInputWords: Array[String] ): Boolean = {
    userInputWords.length == 3 &&
    userInputWords(1).toLowerCase() == Commands.PLAYER &&
      userInputWords(2).isInstanceOf[String]
  }

  private def isValidMoveUserCommand( userInputWords: Array[String] ): Boolean = {
    (  ( userInputWords.length == 4
      && userInputWords(2).replace( "," ,"" ).forall( _.isDigit )
      && userInputWords(3).replace( "," ,"" ).forall( _.isDigit )
      ) || ( userInputWords.length == 2 )
      ) && userInputWords(1).isInstanceOf[String]
  }


  private def invalid( string: String ): Unit = {
    TerminalDisplay.error( s"Input is not valid -> $string" )
    TerminalDisplay.info( "Type help OR ? to see options")
  }



}
