
/*
 * Copyright: blackbox-test -- created 25.04.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object MoveManager {


  private val POSITIONS_GOOSE = Array( 5, 9, 14, 18, 23, 27)
  private val POSITION_WIN = 63
  private val POSITION_START = 0
  private val POSITION_BRIDGE_START = 6
  private val POSITION_BRIDGE_END = 12



  def movePlayer( playerName: String, die1: Int, die2: Int ): Unit = {


    val curPlayerPos: Int = PlayerManager.getPlayerPosition(playerName)
    val playerStartFromString = if (curPlayerPos == 0) {
      "Start"
    } else {
      curPlayerPos
    }

    var nextPlayerPos = curPlayerPos + die1 + die2
    var outputString: String = s"$playerName rolls $die1, $die2. $playerName moves from $playerStartFromString to "
    var continueMove: Boolean = true

    var gameCompleted = false


    if (curPlayerPos == POSITION_WIN)
      TerminalDisplay.info(s"$playerName has already finished the game.")
    else {

          while (continueMove) {

            continueMove = false

            nextPlayerPos match {


              case goose if POSITIONS_GOOSE.contains(goose) =>
                outputString += s"${nextPlayerPos}, The Goose. $playerName moves again and goes to "
                nextPlayerPos += (die1 + die2)
                continueMove = true

              case bridge if bridge == POSITION_BRIDGE_START =>
                nextPlayerPos = POSITION_BRIDGE_END
                outputString += s"The Bridge. $playerName jumps to "
                continueMove = true

              case bounce if bounce > POSITION_WIN =>

                outputString += s"${nextPlayerPos}. $playerName bounces! $playerName returns to "
                nextPlayerPos = POSITION_WIN - (nextPlayerPos - POSITION_WIN)
                continueMove = true

              case win if win == POSITION_WIN =>
                nextPlayerPos = POSITION_WIN
                outputString += s"${nextPlayerPos}. $playerName wins!!\n Type Exit to close the game"
                gameCompleted = true
              case _ =>
                outputString += s"${nextPlayerPos}."

            }

            val otherPlayerPositions = PlayerManager.getPlayers.filter(player => PlayerManager.getPlayerPosition(player) == nextPlayerPos)

            if ( !continueMove  // if move is continuing then wait until player has landed on final square
              && otherPlayerPositions.nonEmpty //there should be 1 other player on the landing square
              &&  otherPlayerPositions.head != playerName //the player should not the moving player ( bounce condition )
            ) {

              val otherPlayer = otherPlayerPositions.head
              outputString += s" On $nextPlayerPos there is ${otherPlayer}, who returns to $curPlayerPos"
              PlayerManager.move(otherPlayer, curPlayerPos)
            }


          }

          PlayerManager.move( playerName, nextPlayerPos )
          if ( gameCompleted ) TerminalDisplay.success( outputString ) else TerminalDisplay.info( outputString )
        }
      }



  }
