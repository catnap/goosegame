import scala.io.StdIn

/*
 * Copyright: blacktest -- created 24.04.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object TerminalDisplay {


  def getUserInput: String = {
    info( "\nYour input -> ")
    StdIn.readLine()
  }


  // Output formats with different colors
  def info( string: String ) = {output( Console.BLUE + s"$string")}
  def success( string: String ) = {output( Console.GREEN + s"$string")}
  def warning( string: String ) = { output( Console.YELLOW + s"Warning: $string ") }
  def error( string: String ) = { output( Console.RED + s"Error: $string ")}

  def help = {
    info(
      """
        Available Commands
        -> add player {playerName}          -> add bob
        -> move {playerName}                -> move bob
        -> move {playerName} {dice rolls}   -> move bob 1 3
        -> exit
        -> help OR ?

        The Goose Game Rules can be found here -> https://en.wikipedia.org/wiki/Game_of_the_Goose
      """.stripMargin)
  }


  // Just a wrapper, maybe logging at some point?
  private def output( string: String ) = { println( string ) }

}
