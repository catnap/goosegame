/*
 * Copyright: blackbox-test -- created 24.04.19
 * Documentation: https://devhat.atlassian.net/wiki/spaces/ADM/overview
 */


object Commands {


  val ADD = "add"
  val PLAYER = "player"
  val MOVE = "move"
  val QUESTION_MARK = "?"
  val HELP = "help"
  val EXIT = "exit"

}
